/*
 * CFile1.c
 *
 * Created: 21.1.2020. 21:37:54
 *  Author: Matej
 */ 


int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	PORTF.DIRSET= PIN5_bm;

	/* Replace with your application code */
	while (1) {
		PORTF.OUTTGL= PIN5_bm;	
		_delay_ms(200);
	}
	
}
