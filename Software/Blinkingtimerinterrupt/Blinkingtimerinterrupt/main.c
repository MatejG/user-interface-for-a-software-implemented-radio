#include <atmel_start.h>
#include "interrupt_avr8.h"
#include "util/delay.h"

volatile int flag;
int main(void)
{
	flag=0;
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	PORTF.DIRSET= PIN5_bm;
	TCA0.SINGLE.CTRLA= (0x06)<<1;     //DIV 256
	TCA0.SINGLE.INTCTRL= RTC_OVF_bm;
	TCA0.SINGLE.PER = 26041;     //every 2 seconds interrupt
	PORTF.OUTSET= PIN5_bm;   
	_delay_ms(4000);
	PORTF.OUTCLR= PIN5_bm;   //turned off at first 
	
	TCA0.SINGLE.CTRLA |= 1; 
	
	
	
	sei();

	/* Replace with your application code */
	while (1) {
	}
	

	
}


ISR( TCA0_OVF_vect){
	
	uint8_t intflags=TCA0.SINGLE.INTFLAGS;
	TCA0.SINGLE.INTFLAGS=intflags;
	
	if( flag == 0){
		PORTF.OUTTGL= PIN5_bm;
		_delay_ms(200);
		PORTF.OUTTGL= PIN5_bm;
		_delay_ms(200);
		PORTF.OUTTGL= PIN5_bm;
		_delay_ms(200);
		PORTF.OUTTGL= PIN5_bm;
	}
	else if ( flag==1){
		PORTF.OUTTGL= PIN5_bm;
		_delay_ms(800);
		PORTF.OUTTGL= PIN5_bm;
	}
		
	flag= 1-flag;
	
	
	
}
