#include <atmel_start.h>
#include <ispis.h>

/**
 * Initializes MCU, drivers and middleware in the project
 **/
void atmel_start_init(void)
{

	uart_init();
	
	
	// setup timed interrupts  every 50ms
	TCA0.SINGLE.CTRLA= 0x0A;          //DIV64
	TCA0.SINGLE.INTCTRL= RTC_OVF_bm;        
	TCA0.SINGLE.PER = 2605;
	
	//setup encoder timer every 10 ms

	TCB0.CCMP=33333; 
	TCB0.INTCTRL=TCB_ENABLE_bm;  
	
	//setup serial interrupt every second
	
	TCB1.CTRLA|=(1<<2);       // same DIV as TCA
	TCB1.CCMP=52083;                 
	TCB1.INTCTRL=TCB_ENABLE_bm;
	
	//set encoder ports
	
	PORTB.DIRCLR= PIN2_bm;    //input pins
	PORTB.DIRCLR= PIN3_bm;
	PORTB.PIN2CTRL= PORT_PULLUPEN_bm;    //OUTPUT A , blue wire CLK
	PORTB.PIN3CTRL= PORT_PULLUPEN_bm;    //OUTPUT B , purple wire  DATA
	
	//set output rows  on logic 0 
	
	PORTD.DIRSET=PIN4_bm;
	PORTD.DIRSET=PIN5_bm;
	PORTD.DIRSET=PIN6_bm;
	PORTD.DIRSET=PIN7_bm;
	
	PORTD.OUTCLR=PIN4_bm;
	PORTD.OUTCLR=PIN5_bm;
	PORTD.OUTCLR=PIN6_bm;
	PORTD.OUTCLR=PIN7_bm;
	
	// setup input pins 
	
	PORTD.DIRCLR =  PIN0_bm;
	PORTD.DIRCLR =  PIN1_bm;
	PORTD.DIRCLR =  PIN2_bm;
	PORTD.DIRCLR =  PIN3_bm;
	
	PORTD.PIN0CTRL= PORT_PULLUPEN_bm;
	PORTD.PIN1CTRL= PORT_PULLUPEN_bm;
	PORTD.PIN2CTRL= PORT_PULLUPEN_bm;
	PORTD.PIN3CTRL= PORT_PULLUPEN_bm;
	printString("Starting\n");
	
		

	system_init();
}
