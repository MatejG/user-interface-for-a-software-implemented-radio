/*
 * IncFile1.h
 *
 * Created: 15-Jan-20 7:14:54 PM
 *  Author: MatejG
 */ 


#ifndef INCFILE1_H_
#define INCFILE1_H_





#endif /* INCFILE1_H_ */


#include <avr/io.h>
#include <util/delay.h>

void uart_init(void);
void uart_on(void);
void uart_off(void);
void uart_putchar(char c);
void printChar( char  c);
void printString( char  * c);