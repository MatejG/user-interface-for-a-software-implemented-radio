#include <atmel_start.h>
#include "util/delay.h"
#include <stdio.h>
#include <string.h>
#include <ispis.h>
#include <interrupt_avr8.h>
#include <keyboard.h>
#include <lcd.h>



#define  OUTPUT_A (PORTB.IN & 4)
#define  OUTPUT_B ((PORTB.IN & 8)>>1)   //shifted right so it can be compared to  output_A
#define  INCREMENT 0
#define  DECREMENT 1


volatile int lastColumn ;
volatile char  button;
volatile char podatci[]={'0','0','0','0','0','0','0','0','0','0'};
volatile unsigned char index;
volatile unsigned char aLastState;
volatile unsigned char encoderStatus;     // 2..neutral no change, 0 increment, 1 decrement






void updateData(unsigned char mode, unsigned char startIndex){      //used for rotary encoder 
	unsigned char i;
	unsigned char border;
	unsigned char found=0;
	
	switch(mode){
		
		case 0:         //increment
		
			if(startIndex==0 && podatci[0]!='9'){
				podatci[0]++;
				lcd_putc(podatci[0]);
				break;
			}
			for(i=startIndex;i>=0;i--){      //find digit to increment if 9 (solves 99999 case)
				if(podatci[i]!='9'){        //if not 9 or . 
					if(podatci[i]=='.')
						continue;
					border=i;
					found=1;
					break;
				}
			
			}  
			if(found==0)
				return;
			move_cursor_xy(border,0);
			
			for(i=border;i<=startIndex;i++){    //increment values from border to starting index 
				if(podatci[i]!='.') {  //skip dot
					
					podatci[i]++;
					if(podatci[i]=='9'+1)
						podatci[i]='0';
				}
				lcd_putc(podatci[i]);
			}
			break;
			
		
		
		
		case 1:       //decrement
	
			if(startIndex==0 && podatci[0]!='0'){
				podatci[0]--;
				lcd_putc(podatci[0]);
				break;
			}
			for(i=startIndex;i>=0;i--){      //find digit to decrement if 0 (solves 0000 case)
				if(podatci[i]!='0'){        //if not highest digit and 0
					if(podatci[i]=='.')
						continue;
					border=i;
					found=1;
					break;
				}
				
			}
			if(found==0)
				return;
			move_cursor_xy(border,0);
			
			for(i=border;i<=startIndex;i++){     //decrement values from border to starting index 
				if(podatci[i]!='.'){
					podatci[i]--;
					if(podatci[i]=='0'-1)
						podatci[i]='9';
				}
				lcd_putc(podatci[i]);
				
			}
			break;

		default:
			return;
		
	}
	move_cursor_xy(startIndex,0);   //return to original index
	
}



void parseBuffer(char input_buffer[], unsigned char mode, unsigned char inputCounter){
	
	unsigned char i;
	int j;

	lcd_clrscr();
	
	for(i=0;i<=9;i++)
		podatci[i]='0';

		
		
	
	switch(mode){
		
		case 0:
	
			podatci[3]='.';
			j=3-inputCounter;
			if(j<0)
				j=0;
	
			for(i=0;i<=9;i++){
				if(input_buffer[i]!='.'){
					podatci[j]=input_buffer[i];
					j++;
					if(j==3)
						j++;
					if(j==9)
						break;
					
				}	
			}
			
			
			for(i=0;i<=9;i++)
				lcd_putc(podatci[i]);
			lcd_writeString(" MHz");
			move_cursor_xy(2,0);
			index=2;
				
			break;
			
		case 1:
		
			podatci[6]='.';
			j=6-inputCounter;
			
			if(j<0)
				j=0;
			
				
			for(i=0;i<=9;i++){
				if(input_buffer[i]!='.'){
					podatci[j]=input_buffer[i];
					j++;
					if(j==6)     //skip dot in podatci
						j++;
					if(j==9)
						break;
						
				}

			}
					
			for(i=0;i<=9;i++)
				lcd_putc(podatci[i]);
			lcd_writeString(" KHz");
			move_cursor_xy(5,0);
			index=5;
			break;
		
	}
}

int main(void)
{
    lastColumn=-1;   
	button = 'K';
	index=0;
	aLastState=OUTPUT_A;
	encoderStatus=3;
	
	unsigned char dot=0;
	unsigned char i=0;
	unsigned char active=0;    //1 - keyboard in new input mode, 0 attending current input cycle
	unsigned char input_buffer[10];
	unsigned char inputCounter=0;
	
		/* Initializes MCU, drivers and middle ware */
	atmel_start_init();
	
	sei();
	TCA0.SINGLE.CTRLA |= 1; //activate  keypad timer
	TCB0.CTRLA&=(~(TCB_ENABLE_bm));  //deactivate encoder initially
	TCB1.CTRLA|=TCB_ENABLE_bm;
	lcd_init();
	lcd_writeString("     START");
	lcd_home();
	
	
	while (1) {
		while(button =='K'){
			
			if(active ==0){
				updateData(encoderStatus,index);
				encoderStatus=2;                    //reset global variable
				
				
				}
			
			}                 
			
			        
		while(!(PORTD.IN & (1<<lastColumn)));  //while not let go 
		
		switch(button){
			case 'C':
				if(index!=0 && active==0){
					lcd_command(LCD_MOVE_CURSOR_LEFT);  
					index--;
					if(podatci[index]=='.'){
						lcd_command(LCD_MOVE_CURSOR_LEFT);  
						index--;
						}
					}   
				break;
			case 'D':
				if(index!=9 && active==0){
					lcd_command(LCD_MOVE_CURSOR_RIGHT);
					index++;
					if(podatci[index]=='.'){
						lcd_command(LCD_MOVE_CURSOR_RIGHT);
						index++;
						}
					}
				break;
			case 'A':        
				if(active==1) {                       
					parseBuffer(input_buffer,0,inputCounter);
					active=0;
					dot=0;
					TCB0.CTRLA|=TCB_ENABLE_bm;  //activate encoder counter
					
				}
				break;
			case 'B':
				if(active==1) {
					parseBuffer(input_buffer,1,inputCounter);
					active=0;
					dot=0;
					TCB0.CTRLA|=TCB_ENABLE_bm;
				}
				break;
		
			case '*':
				if(active==1 && dot==0 && index!=9){       //ensures the dot cant be in first place
					lcd_putc('.');
					dot=1;
					input_buffer[index]='.';
					index++;
					}
				break;

			case '#':
				if(active==0){

					for(i=index+1;i<=9;i++){
						
						if(podatci[i]!='.'){
							
							podatci[i]=0;
							move_cursor_xy(i,0);
							lcd_putc('0');

							}
						}
					move_cursor_xy(index,0);
					}
				break;
				
			default:
			
				if(active==0){
					active=1;
					inputCounter=0;
					TCB0.CTRLA&=(~(TCB_ENABLE_bm));  //deactivate encoder counter
					lcd_clrscr();
					index=0;
					for(i=1;i<=9;i++)
						input_buffer[i]='0';
						
						
			
					input_buffer[0]=button;
					lcd_putc(button);
					inputCounter++;
					index++;
				}else{
					
					input_buffer[index]=button;
					lcd_putc(button);
					
					if(index==9)
						move_cursor_xy(9,0);   //don't let it go to HZ part
					else{
						index++;
						if(dot==0)
							inputCounter++;
						}
					
				}
				
				
				
				
				
				
		} //end of switch case
		button='K';                //reset the key 
		lastColumn= -1;
		
	}// end of while
	
}


//KEYBOARD INTERRUPT 
ISR(TCA0_OVF_vect){  
	uint8_t column ;
	uint8_t  row=10;
	uint8_t i;
	uint8_t intflags = TCA0.SINGLE.INTFLAGS;
	TCA0.SINGLE.INTFLAGS= intflags;
	
		
	for(i=0; i<4; i++) {   //find out which column is pressed
		if(!(PORTD.IN & (1<<i)) ) {
			if ( lastColumn==i ){  //second iteration found the same column 
				column = i+1;
				break;
			}
			else {
				lastColumn=i;     //first appearance of a button 
				return;
				
			}
		}
		if(i==3){
			lastColumn= -1;    //if no button is pressed 
			return;   
		}
		
	}  //end for loop 

	
	for( i=4; i<8 ;i++){	 //go through the rows
		PORTD.OUTSET = (1<<i);   //set output to 1 to find the row   
		_delay_us(1);                  //delay for signal stabilising 
		if(PORTD.IN & (1<<lastColumn)){    //row check 
			row = i-3;
			PORTD.OUTCLR = (1<<i);
			break;
		}
		PORTD.OUTCLR = (1<<i);
	}  //end for
	
	
	
	button = parseNumber(row,column);   // find out what button was pressed 	 
	return;

	
	
}


// ROTARY ENCODER 
ISR(TCB0_INT_vect ){
	
	uint8_t intflags = TCB0.INTFLAGS;
	TCB0.INTFLAGS= intflags;
	
	unsigned char aState;
	unsigned char bState;
	
	aState = OUTPUT_A ; // Reads the "current" state of the outputA
	// If the previous and the current state of the outputA are different, that means a Pulse has occured

	
	if (aState!=aLastState){         
		
		bState= OUTPUT_B; 
		if (bState != aState) 
			encoderStatus=INCREMENT;
		else 
			encoderStatus=DECREMENT;
		aLastState = aState;   // Updates the previous state of the outputA with the current state
		}
		return;
}



//SERIAL REFRESH ROUTINE
ISR(TCB1_INT_vect){
	uint8_t intflags = TCB1.INTFLAGS;
	TCB1.INTFLAGS= intflags;
	unsigned char i;
	unsigned char trailing=1;
	for(i=0;i<=9;i++){
		
		if(trailing==1 && podatci[i]!='0')     //skip trailing zeroes
			trailing=0;
		
		if(trailing==0)
			printChar(podatci[i]);
	}
	if(trailing==0)
		printChar('\n');
}
	
	

