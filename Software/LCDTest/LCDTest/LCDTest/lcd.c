
#include <atmel_start.h>
#include "util/delay.h"



//data from PC0:PC3


#define RS_bm (1<<4)
#define EN_bm  (1<<5)
#define LCD_PORT  VPORTC_OUT                           //define which port the LCD is connected to 
#define LCD_DIR VPORTC_DIR


unsigned char _lcd_maxx=16;
unsigned char _lcd_x,_lcd_y;
static unsigned char _base_y[]= {0x00,0x28};    //define first line digits 



static void write_chunk(unsigned char c)
{
	LCD_PORT &= RS_bm;    //keep RS, RW=0, EN=0, data=0
	LCD_PORT |= (c | EN_bm);        //EN=1, data=c
	_delay_us(1);
	_delay_us(1);
	LCD_PORT &= ~EN_bm;           //EN=0 falling edge latches data
}

void lcd_data(unsigned char data){
	unsigned char d=50;
	LCD_PORT |= RS_bm;             // RS=1
	write_chunk(data >> 4);       //hi
	write_chunk(data & 0x0F);     //lo
	while (d--)// regular data or cmd
	{
		_delay_us(1);
	}                    
}


 void lcd_command(char cmd){
	unsigned char d=50;
	LCD_PORT &= ~RS_bm;             // RS=0
	write_chunk(cmd >> 4);      //top 4 bits first
	write_chunk(cmd & 0x0F);     
	while (d--)                    //wait between instructions
	{
		_delay_us(1);
	}                 // regular data or cmd
	if (cmd <= 2){
		d=2;
		while(d--)
			_delay_ms(1);    // CLS and HOME
		
		} 
}




void move_cursor_xy(unsigned char x, unsigned char y){        //(0,0) is the top left corner , (15,1) is the bottom right corner
	unsigned char calc;
	if(x>16 || x<0)
		return;
	switch(y){
		
		case 0:
			lcd_command(0x80 | (0x00 + x));
			_lcd_x=x;
			_lcd_y=y;
			break;
		case 1:
			lcd_command(0x80 | (0x28 + x));
			_lcd_x=x;
			_lcd_y=y;
			break;
		default:
			return;
	}
	
	
}


void lcd_clrscr(void){
	
	lcd_command(0x01);
	_lcd_x=0;
	_lcd_y=0;
}


void lcd_home(void){
	
	lcd_command(0x02);
	_lcd_x=0;
	_lcd_y=0;
}

void lcd_putc(char c){
	
	if (c == '\n')
	{	
		move_cursor_xy(0,(++_lcd_y)%2);
	}
	if (c != '\n') {
		++_lcd_x;
		lcd_data(c);
	}
}

void lcd_writeString(const char *str)
{
	while (*str) lcd_putc(*str++);
}


void lcd_Custom_Char (unsigned char loc, unsigned char *msg)      //custom character creation
{
	unsigned char i;
	if(loc<8)
	{
		lcd_command(0x40 + (loc*8));	
		for(i=0;i<8;i++)	
			lcd_data(msg[i]);
	}
}


void lcd_init(){
	
	LCD_DIR = 0xFF;
	LCD_PORT = 0;                   //set operating mode, cursor settings, cursor shift settings and clear screen
	unsigned char init_sequence[] = { 0x28, 0x0E, 0x06, 0x01 };     //list of commands to configure the display
	_lcd_x=0;
	_lcd_y=0;
	_delay_ms(30);    
	lcd_command(0x30);
	_delay_ms(5);
	lcd_command(0x30);
	_delay_us(100);
	lcd_command(0x30);
	_delay_us(50);
	lcd_command(0x20);
	_delay_us(50);
	lcd_command(0x20);   
	for (unsigned  char i =0; i<sizeof(init_sequence);i++)
		lcd_command(init_sequence[i]);
	
}


void calm(){
	
	lcd_writeString("See...pretty :P\n");
	
	for(unsigned char i=0;i<15;i++){
		if (i %2==0)
			lcd_putc(1);
		else
			lcd_putc(' ');
		
	}

	
	
}