/*
 * lcd.h
 *
 * Created: 03-Jun-20 6:18:05 PM
 *  Author: MatejG
 */ 


#ifndef LCD_H_
#define LCD_H_




#define LCD_MOVE_CURSOR_LEFT     0x10   /* move cursor left  (decrement)          */
#define LCD_MOVE_CURSOR_RIGHT    0x14   /* move cursor right (increment)          */
#define LCD_MOVE_DISP_LEFT       0x18   /* shift display left                     */
#define LCD_MOVE_DISP_RIGHT      0x1C   /* shift display right                    */



void lcd_data(unsigned char data);
void lcd_command(char cmd);
void move_cursor_xy(unsigned char x, unsigned char y);
void lcd_clrscr(void);
void lcd_home(void);
void lcd_putc(char c);
void lcd_writeString(const char *str);
void lcd_init();
void lcd_Custom_Char (unsigned char loc, unsigned char *msg) ;



#endif /* LCD_H_ */