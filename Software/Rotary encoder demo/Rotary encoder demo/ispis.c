#define F_CPU 3333333ul
#include <avr/io.h>
#include <util/delay.h>


//USART na portb pin 0 za slat podatke na komp TX

#define BAUDRATE 9600
#define BAUD_FROM_RATE(RATE) ((4UL * (F_CPU))/(RATE))

void uart_init(void)
{
	USART3.BAUD = BAUD_FROM_RATE(BAUDRATE);
	USART3.CTRLC =  USART_CHSIZE_8BIT_gc;

}

void uart_on(void)
{
	// Enable TX
	USART3.CTRLB = USART_TXEN_bm;
	// TX pin to output
	PORTB.DIRSET = (1 << 0);
	PORTB.PIN0CTRL = PORT_PULLUPEN_bm;
	

}

void uart_off(void)
{
	// Disable TX
	USART3.CTRLB &= ~USART_TXEN_bm;     //onesposobi uart odasiljanje
}

void uart_putchar(char c){
	// Wait for space
	uart_on();
	while (!(USART3.STATUS & USART_DREIF_bm))  //ispitivanje zastavice dal je poslan bit
	;
	
	if(c =='/n'){    //konvencija kaze da prije /n ide znak /r
		USART3.TXDATAL ='/r';
		while (!(USART3.STATUS & USART_DREIF_bm));
	}
	
	USART3.TXDATAL = c;   //slanje podatka
	uart_off();
}




void print( char * c){
	
	while(*c){
		uart_putchar(*(c));
		c++;
		
	}
	
}