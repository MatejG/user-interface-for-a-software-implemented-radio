#include <atmel_start.h>
#include <ispis.h>
#define  OUTPUT_A  ((PORTB.IN & 4) >>2)
#define  OUTPUT_B  ((PORTB.IN & 8)>>3)   //shifted right so it can be compared to  output_A




int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	uart_init();

    unsigned char counter = '0';
	unsigned char aLastState;
	unsigned char test=0;
	uint16_t state=0x0000;
	
	unsigned char aState;
	unsigned char bState;
	print("Start");
	print("\n");

	PORTB.DIRCLR= PIN2_bm;    //input pins
	PORTB.DIRCLR= PIN3_bm;
	PORTB.PIN2CTRL= PORT_PULLUPEN_bm;    //OUTPUT A , blue wire CLK
	PORTB.PIN3CTRL= PORT_PULLUPEN_bm;    //OUTPUT B , purple wire  DATA

	aLastState=OUTPUT_A;
	
	while(1) {	
	
		
		aState = OUTPUT_A ; // Reads the "current" state of the outputA
		
		if(aState != aLastState)
			test++;
		else
			test=0;	
			
		if (test==12){         // 12 iterations of same reading are required to be valid 
			
			bState= OUTPUT_B;
			if (bState != aState)
				uart_putchar('D');
			else
				uart_putchar('L');
			aLastState = aState;   // Updates the previous state of the outputA with the current state
			test=0;
		}
		
	}
		
}
	
	
	
	
	
	
